package lt.sda.academy.data;

import lt.sda.academy.model.Author;
import lt.sda.academy.presentation.AuthorChoice;

public class AuthorValidator {
    public static boolean validateAuthor(Author authorToValidate) {
        String containsNumberRegex = ".*\\d.*";
        if (isNullOrEmpty(authorToValidate.getName()) && authorToValidate.getName().contains(containsNumberRegex)) {
            AuthorChoice.authorIsNotValid(authorToValidate, "name");
            return false;
        }
        if (isNullOrEmpty(authorToValidate.getFamilyName()) && authorToValidate.getFamilyName().contains(".*\\d.*")) {
            AuthorChoice.authorIsNotValid(authorToValidate, "family name");
            return false;
        }
        return true;
    }
    private static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }
}
