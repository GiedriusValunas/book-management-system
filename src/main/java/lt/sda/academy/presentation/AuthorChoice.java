package lt.sda.academy.presentation;

import lt.sda.academy.model.Author;
import lt.sda.academy.util.ScannerUtil;

import java.util.Collection;

public class AuthorChoice {
    public static int selectAuthorsChoices() {
        System.out.println("1. Matyti visus autorius");
        System.out.println("2. Ieškoti autorių pagal vardą arba pavardę ");
        System.out.println("3. Pridėti autorių");
        System.out.println("4. Atnaujinti autorių ");
        System.out.println("5. Ištrinti Autorių pagal id");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static Author createAuthorLines() {
        System.out.println("Iveskite Autoriaus vardą: ");
        String authorName = ScannerUtil.SCANNER.next();
        System.out.println("Iveskite Autoriaus pavard3: ");
        String authorFamilyName = ScannerUtil.SCANNER.next();
        return new Author(authorName, authorFamilyName);
    }

    public static void printAllAuthorInfo(Collection<Author> authorCollection) {
        authorCollection.stream()
                .map(Author::toStringIdNameAndFamilyName)
                .forEach(System.out::println);
    }

    public static String findAuthorBySomething() {
        System.out.println("Įveskite ieškomą frazę: ");
        return ScannerUtil.SCANNER.next();
    }

    public static void authorIsNotValid(Author author, String badField) {
        System.out.println("Author: " + author.toStringNameAndFamilyName() + "because of: " + badField);
    }

    public static void authorWasSavedMessage(Author author) {
        crudAuthorSystemOut(author, "saved");
    }

    public static void authorWasUpdatedMessage(Author author) {
        crudAuthorSystemOut(author, "update");
    }

    public static void authorWasDeletedSuccessfully(Author author) {
        crudAuthorSystemOut(author, "deleted");
    }

    private static void crudAuthorSystemOut(Author author, String operation) {
        System.out.printf("Author %s was %s successfully \n", author.toStringIdNameAndFamilyName(), operation);
    }

    public static long insertAuthorIdMessage() {
        System.out.println("Įveskite autoriaus id: ");
        return ScannerUtil.SCANNER.nextLong();
    }

    public static void authorWasNotFoundById(Long id) {
        System.out.println("Author by id: " + id + " was not found");
    }

}
